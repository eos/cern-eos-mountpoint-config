# cern-eos-mountpoint-config

Experimental package for automatic "/eos" autofs configuration.

### Notice

The latest maintainers of this project are the FTS team:
- `0.0.5` tag (brings `/eos/workspace` mount)
- `0.0.6` tag (EOS5 + Alma9 RPMs)

This project is considered deprecated and the [eos/cern-eos-mount][1] project 
should be used instead.

[1]: https://gitlab.cern.ch/eos/cern-eos-mount
