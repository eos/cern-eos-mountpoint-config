#!/usr/bin/env bash
set -e

if [[ -f /usr/bin/dnf ]]; then
  dnf install -y dnf-plugins-core git rpm-build tree
else
  yum install -y yum-utils git rpm-build tree python3
fi
