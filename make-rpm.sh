#!/usr/bin/env bash

./make-srpm.sh
cd build/

if [[ -f /usr/bin/dnf ]]; then
  dnf builddep -y SRPMS/*
else
  yum-builddep -y SRPMS/*
fi

rpmbuild --rebuild --with server --define "_build_name_fmt %%{NAME}-%%{VERSION}-%%{RELEASE}.%%{ARCH}.rpm" SRPMS/*
